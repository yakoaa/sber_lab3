import CustomException.CustomErrorException;
import CustomException.CustomIOException;
import model.ClassForSerialize;
import model.RecursionClass;

import java.io.*;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Сериализация");

        /*привет Ягудину Руслану*/
        ClassForSerialize clazz = new ClassForSerialize("wow, such string", 14);

        //при помощи обмена файлами

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("clazz.out"));
        objectOutputStream.writeObject(clazz);
        objectOutputStream.close();


        //восстановление класса

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("clazz.out"));
        ClassForSerialize restoredClazz = (ClassForSerialize) objectInputStream.readObject();

        objectInputStream.close();

        //проверка на соответствие классов

        System.out.println(clazz.toString());
        System.out.println(restoredClazz.toString());
        System.out.println(String.format("Классы равны друг другу передача через файл? \n 'equals' = %s, '==' = %s", clazz.equals(restoredClazz), clazz == restoredClazz));

        //при помощи битстримера

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream1 = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream1.writeObject(clazz);

        //восстановление

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream1 = new ObjectInputStream(byteArrayInputStream);
        ClassForSerialize restoredClazz1 = (ClassForSerialize) objectInputStream1.readObject();

        objectInputStream1.close();

        //проверка на соответствие классов 2
        System.out.println(clazz.toString());
        System.out.println(restoredClazz1.toString());
        System.out.println(String.format("Классы равны друг другу byteArraySteam? \n 'equals' = %s, '==' = %s", clazz.equals(restoredClazz1), clazz == restoredClazz1));

        Scanner in = new Scanner(System.in);

        System.out.println("Введите 1(unchecked) или 2(checked) или 3(продолжить работу)");

        System.out.println("Исключения");
        switch (in.nextInt()){
            case 1:

                try {
                    System.out.println("1 case, try to get file, that not existed in system.");

                ObjectInputStream objectInputStream2 = new ObjectInputStream(new FileInputStream("notExistFile.out"));
                objectInputStream2.readObject();
                objectInputStream2.close();
                }catch (IOException e){
                    throw new CustomIOException(e.getMessage());
                }
                break;
            case 2:
                try {
                    System.out.println("2 case, unlimited recursion");
                    System.out.println(RecursionClass.sum(1, 2));
                }catch (Error err)
                {
                    throw new CustomErrorException(err.getMessage());
                }
                break;
            case 3:
                System.out.println("3 case, continue.");
                break;
            default:
                System.out.println("default case, continue.");
                break;
        }
        System.out.println("Рефлексия");

        Collection strings = new ArrayList<String>(){};
        strings.add("first string");
        //черная магия
        TypeVariable[] typeVariables = strings.getClass().getTypeParameters();
        // vr1 = typeVariables();
        //Type[] vr2 = strings.getClass().getGenericInterfaces();
        Type vr3 = strings.getClass().getGenericSuperclass();
        //System.out.println(vr1);
        //System.out.println(vr2);
        System.out.println(vr3);
    }
}
