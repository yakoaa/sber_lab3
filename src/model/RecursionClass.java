package model;

public class RecursionClass {
    private int a;
    private int b;

    public RecursionClass() {
    }

    public static int sum(int a, int b){
        return sum(a + b, b - a);
    }
}
