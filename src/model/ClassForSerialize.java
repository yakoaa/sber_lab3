package model;

import java.io.Serializable;


public class ClassForSerialize implements Serializable {
    private String someString;
    private Integer someInt;

    public ClassForSerialize(String someString, Integer someInt) {
        this.someString = someString;
        this.someInt = someInt;
    }

    public String getSomeString() {
        return someString;
    }

    public void setSomeString(String someString) {
        this.someString = someString;
    }

    public Integer getSomeInt() {
        return someInt;
    }

    public void setSomeInt(Integer someInt) {
        this.someInt = someInt;
    }

    @Override
    public String toString() {
        return String.format("ClassForSerialize={someString = '%s', someInt = '%s'};", someString, someInt);
    }
}
